require 'test_helper'

class BonafidesControllerTest < ActionController::TestCase
  setup do
    @bonafide = bonafides(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bonafides)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bonafide" do
    assert_difference('Bonafide.count') do
      post :create, bonafide: { approved_by: @bonafide.approved_by, collected: @bonafide.collected, print: @bonafide.print, reason: @bonafide.reason, student_id: @bonafide.student_id, year: @bonafide.year }
    end

    assert_redirected_to bonafide_path(assigns(:bonafide))
  end

  test "should show bonafide" do
    get :show, id: @bonafide
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bonafide
    assert_response :success
  end

  test "should update bonafide" do
    put :update, id: @bonafide, bonafide: { approved_by: @bonafide.approved_by, collected: @bonafide.collected, print: @bonafide.print, reason: @bonafide.reason, student_id: @bonafide.student_id, year: @bonafide.year }
    assert_redirected_to bonafide_path(assigns(:bonafide))
  end

  test "should destroy bonafide" do
    assert_difference('Bonafide.count', -1) do
      delete :destroy, id: @bonafide
    end

    assert_redirected_to bonafides_path
  end
end

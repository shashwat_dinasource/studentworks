namespace :db do
	task :backup do
		system "mysqldump --opt --user=root learn_development>database_backup.sql"
  end


	task :restore do
		system "mysqldump --user=root < database_backup.sql"
  end
end

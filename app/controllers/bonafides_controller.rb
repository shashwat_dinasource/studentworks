class BonafidesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource :class => false
  layout :resolve_layout

  def index
    
  end

  def form
    @profile = Student.find_by_student_id(current_user.username)
  end

  def status
      @bonafides = Student.find_by_student_id(current_user.username).bonafides
      respond_to do |format|
        format.html
      end
  end

  def create
    @bonafide = Student.find_by_student_id(current_user.username).bonafides.new(params[:bonafide])
    respond_to do |format|
      if @bonafide.save
        format.html { redirect_to :controller => 'bonafides'}
        flash[:notice] = 'Your leave has been sent for approval'
      end
    end
  end 

  def bonafidemanager
     @bonafides ||= Array.new
    Bonafide.all.each do |list|
      if list.approved.nil?
        @bonafides.push(list)
      end
    end
  end

  def pending
    @page=params[:query].to_i
    @bonafides ||= Array.new
    Bonafide.all.each do |list|
      if list.approved.nil?
        @bonafides.push(list)
      end
    end
    @count=@bonafides.count.to_i
    @bonafides =@bonafides.reverse[@page*8,8]
  end

  def approved
    @page=params[:query].to_i
    @bonafides ||= Array.new
    Bonafide.all.each do |list|
      if (list.approved==true)
        @bonafides.push(list)
      end
    end
    @count=@bonafides.count.to_i
    @bonafides =@bonafides.reverse[@page*8,8]
  end

  def disapproved
      @page=params[:query].to_i
      @bonafides ||= Array.new
      Bonafide.all.each do |list|
       if (list.approved==false)
          @bonafides.push(list)
        end
      end
    @count=@bonafides.count.to_i
    @bonafides =@bonafides.reverse[@page*8,8]
  end

  def approve
    @approve = Bonafide.find(params[:query])
    @approve.approved=true
    @approve.approved_by=current_user.id
    if @approve.save
      redirect_to(:back)
      flash[:notice] = "Bonafide id #{params[:query]} has  been approved"
    end
  end

    def disapprove
    @approve = Bonafide.find(params[:query])
    @approve.approved=false
    @approve.approved_by=current_user.id
    if @approve.save
      redirect_to(:back)
      flash[:notice] = "Bonafide id #{params[:query]} has been Disapproved"
    end
  end

  private
    def resolve_layout
      case action_name
    when "index" , "form", "status"
      "general"
    else
      "admin"
    end
  end
end

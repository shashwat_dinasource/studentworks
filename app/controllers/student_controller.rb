class StudentController < ApplicationController
	before_filter :authenticate_user!
	load_and_authorize_resource
	layout 'general'

	def leave
 
  end
  def leaveform
		@leave =Leave.new
    	respond_to do |format|
    	  	format.html
    	end
	end

	def createleave
    	@leave = Student.find_by_student_id(current_user.username).leaves.new(params[:leave])
    	respond_to do |format|
    	  	if @leave.save
    	    	format.html { redirect_to :controller => 'student'}
    	    	flash[:notice] = 'Your leave has been sent for approval'
    	 	 else
    	 	   format.html { render action: "leave" }
    	  	end
    	end
  	end

  	def leavestatus
  		@leaves = Student.find_by_student_id(current_user.username).leaves
  		respond_to do |format|
  			format.html
  		end
  	end

    def profile
    @profile = Student.find_by_student_id(current_user.username)
    end

    def search
    end

    def expenses
      @expenses= Student.find_by_student_id(current_user.username).expenses
      @total= Expense.total(current_user.username)
    end

    def certificates

    end

    def csa

    end

end

class AnnouncementsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource
  # GET /announcements
  # GET /announcements.json
  def index

  end

  # GET /announcements/1
  # GET /announcements/1.json
  def show
    @announcement = Announcement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @announcement }
    end
  end

  # GET /announcements/new
  # GET /announcements/new.json
  def new
    @announcement = Announcement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @announcement }
    end
  end

  # GET /announcements/1/edit
  def edit
    @announcement = Announcement.find(params[:id])
  end

  # POST /announcements
  # POST /announcements.json
  def create
    @announcement = Announcement.new(params[:announcement])

    respond_to do |format|
      if @announcement.save
        format.html { redirect_to @announcement, announcement: 'Announcement was successfully created.' }
        format.json { render json: @announcement, status: :created, location: @announcement }
      else
        format.html { render action: "new" }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /announcements/1
  # PUT /announcements/1.json
  def update
    @announcement = Announcement.find(params[:id])

    respond_to do |format|
      if @announcement.update_attributes(params[:announcement])
        format.html { redirect_to @announcement, announcement: 'Announcement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /announcements/1
  # DELETE /announcements/1.json
  def destroy
    @announcement = Announcement.find(params[:id])
    @announcement.destroy

    respond_to do |format|
      format.html { redirect_to announcements_url }
      format.json { head :no_content }
    end
  end

  def visible
    @announcement = Announcement.find(params[:id])
    @announcement.is_visible=true
    if @announcement.save
      redirect_to(:back) 
      flash[:announcement] = "Announcement Id #{params[:id]} has  been made visible"
    end
  end

  def invisible
    @announcement = Announcement.find(params[:id])
    @announcement.is_visible=false
    @announcement.is_archived=false
    if @announcement.save
      redirect_to(:back) 
      flash[:announcement] = "Announcement Id #{params[:id]} has  been made invisible"
    end
  end

  def archive
    @announcement = Announcement.find(params[:id])
    @announcement.is_archived=true
    @announcement.is_visible=false
    if @announcement.save
      redirect_to(:back) 
      flash[:announcement] = "Announcement Id #{params[:id]} has been archived"
    end
  end

  def unarchive
    @announcement = Announcement.find(params[:id])
    @announcement.is_visible=true
    @announcement.is_archived=false
    if @announcement.save
      redirect_to(:back) 
      flash[:announcement] = "Announcement Id #{params[:id]} has been non-archived"
    end
  end

    def important
    @announcement = Announcement.find(params[:id])
    @announcement.is_important=true
    if @announcement.save
      redirect_to(:back) 
    end
  end

    def notimportant
    @announcement = Announcement.find(params[:id])
    @announcement.is_important=false
    if @announcement.save
      redirect_to(:back) 
    end
  end
end

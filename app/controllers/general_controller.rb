class GeneralController < ApplicationController
	before_filter :redirect_to_appropriate_role
	layout 'general'

	def index
	end

	def contact
	end

	def sac
	end

	def swdservices
	end
	
	def antiragging
	end

	def migration
	end

	def csa
	end
	
	def announcement
		@announcements= Announcement.all.sort_by{|u| u.created_at}.reverse
	end


	def searchresult
		@search=params[:search]
		@results=Student.find(:all, 
			:conditions => ['name LIKE ? and college_id LIKE? and hostel like ? and room like ? and college_id LIKE?', "%#{@search[:name]}%", "%#{@search[:college_id]}%", "%#{@search[:hostel]}%", "%#{@search[:room]}%", "%#{@search[:branch]}%"])
		#@results=Student.where("name LIKE '%?%' and college_id LIKE '%?%' and hostel LIKE '%?%' and room LIKE '%?%' and college_id LIKE '%?%'",@search[:name],@search[:college_id],@search[:hostel],@search[:room],@search[:branch]).to_sql
	end

	def shownotice
		@notice=Announcement.find(params[:id])
	end

	private

	def redirect_to_appropriate_role
		if user_signed_in?
      		redirect_to url_for controller: current_user.role, action: "index"
      		flash[:notice] = "Please loggout to access this link"
      	end
	end

end

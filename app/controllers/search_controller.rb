class SearchController < ApplicationController
	before_filter :authenticate_user!, :except => [:index, :result]
	authorize_resource :class => false
	layout :resolve_layout
	def index
	
	end

	def result
		@search=params[:search]
		@results=Student.find(:all, 
			:conditions => ['name LIKE ? and college_id LIKE? and hostel like ? and room like ? and college_id LIKE?', "%#{@search[:name]}%", "%#{@search[:college_id]}%", "%#{@search[:hostel]}%", "%#{@search[:room]}%", "%#{@search[:branch]}%"])
	end


	private
    def resolve_layout
      case action_name
    when "index" , "result"
      "general"
    else
      "admin"
    end
  end
end

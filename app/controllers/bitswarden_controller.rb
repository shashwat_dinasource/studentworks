class BitswardenController < ApplicationController
	before_filter :authenticate_user!
	load_and_authorize_resource
	def index
	end

	def approveleave
		@leavelist ||= Array.new
		hostels=User.find(current_user.id).bitswarden.hostel_wardens
		hostels.each do |hostel|
			Student.find_by_hostel(hostel.hostel) do |student|
				Student.find(student.id).leaves.each do |list|
					if list.approved.nil?
						@leavelist.push(list)
					end
				end
			end
		end
	end

	def approvedleave
		@leavelist ||= Array.new
		hostels=User.find(current_user.id).bitswarden.hostel_wardens
		hostels.each do |hostel|
			Student.find_by_hostel(hostel.hostel) do |student|
				Student.find(student.id).leaves.each do |list|
					if (list.approved==true)
						@leavelist.push(list)
					end
				end
			end
		end
	end

		def disapprovedleave
		@leavelist ||= Array.new
		hostels=User.find(current_user.id).bitswarden.hostel_wardens
		hostels.each do |hostel|
			Student.find_by_hostel(hostel.hostel) do |student|
				Student.find(student.id).leaves.each do |list|
					if (list.approved==false)
						@leavelist.push(list)
					end
				end
			end
		end
	end

	def approve
		@approve = Leave.find(params[:query])
		@approve.approved=true
		@approve.bitswarden_id=current_user.id
		if @approve.save
			redirect_to(:back) 
			flash[:notice] = "Leave id #{params[:query]} has  been approved"
		end
	end

		def disapprove
		@approve = Leave.find(params[:query])
		@approve.approved=false
		@approve.bitswarden_id=current_user.id
		if @approve.save
			redirect_to(:back)
			flash[:notice] = "Leave id #{params[:query]} has been Disapproved"
		end
	end
end

class ApplicationController < ActionController::Base
protect_from_forgery
layout 'admin'
rescue_from CanCan::AccessDenied do |exception|
  	if user_signed_in?
    redirect_to url_for controller: current_user.role, action: "index"
    else
    redirect_to root_path
    end  	
  	flash[:notice] = "Your are not allowed to access this link #{controller_name}/#{action_name}"
  end
end

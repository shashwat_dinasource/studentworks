class SessionsController < ApplicationController
  # For security purposes, Devise just authenticates an user
  # from the params hash if we explicitly allow it to. That's
  # why we need to call the before filter below.
  before_filter :allow_params_authentication!, :only => :create
 
  def new
    if user_signed_in?
      redirect_to url_for controller: current_user.role, action: "index"
    else
      @user = User.new(params[:user])
    end
  end
 
  def create
    # Since the authentication happens in the rack layer,
    # we need to tell Devise to call the action "sessions#new"
    # in case something goes bad. Feel free to change it.
    #user = authenticate_user!(:recall => "sessions#new")
    user = User.authenticate(params[:user][:username], params[:user][:password])
    if user
      flash[:notice] = "You are now signed in!"
      sign_in user
      redirect_to url_for controller: user.role, action: "index"
    else
      flash[:notice] = "Unable to sign you in. Username or Password is incorrect"
      redirect_to(:back)
    end
  end
 
  def destroy
    sign_out
    flash[:notice] = "You are now signed out!"
    redirect_to root_path
  end
end
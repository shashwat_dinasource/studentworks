class User < ActiveRecord::Base
  has_one :student, :primary_key => :username, :foreign_key => :student_id #defines foreign key for student table while searching User.find(1).student
  has_one :staff, :foreign_key => :staff_id
  has_one :admin, :foreign_key => :admin_id
  has_one :bitswarden, :foreign_key => :bitswarden_id
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,:username

  def student?
    return role == "student"
  end

  def admin?
    return role == "admin"
  end

  def staff?
    return role == "staff"
  end

  def staff?
    return role == "staff"
  end

  def self.authenticate(username, password)
       user = User.find_for_authentication(:username => username)
       if user
         user.valid_password?(password) ? user : false
       else
         false
       end
  end
 end

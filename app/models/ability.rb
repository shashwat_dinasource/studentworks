class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    
    if user.role == "admin"
        can :manage, Admin
        can :manage, Navigation
        can :manage, Announcement
        can [:approved, :disapproved, :pending, :approve, :disapprove,:bonafidemanager ], :bonafide
    elsif user.role == "student"
        can :manage, Student
        can [ :index, :form, :status, :create ], :bonafide
        can [ :index ], :certificate
        can [:index, :result], :search
    elsif user.role == "staff"
        can :manage, Staff
        can :manage, Announcement
        can [:approved, :disapproved, :pending, :approve, :disapprove,:bonafidemanager ], :bonafide
    elsif user.role == "bitswarden"
        can :manage, Bitswarden
    elsif user.role.nil?
        can [:index, :result], :search
    end
    

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end

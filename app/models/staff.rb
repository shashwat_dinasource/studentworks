class Staff < ActiveRecord::Base
	has_one :user, :foreign_key => :id, :primary_key => :staff_id
  attr_accessible :name, :staff_id
end

class Insurance < ActiveRecord::Base
	belongs_to :student, :primary_key => :student_id #defines primary key in student table while searching Insurance.find(2).student
  attr_accessible :amount, :edate, :password, :policyno, :sdate, :student_id
end

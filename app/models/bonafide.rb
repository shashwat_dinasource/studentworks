class Bonafide < ActiveRecord::Base
	belongs_to :student, :primary_key => :student_id
  attr_accessible :approved, :approved_by, :collected, :print, :reason, :student_id, :year
end

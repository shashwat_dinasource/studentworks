class Announcement < ActiveRecord::Base
  attr_accessible :body, :is_archived, :is_important, :is_visible, :posted_by, :title
end

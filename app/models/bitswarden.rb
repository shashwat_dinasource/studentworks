class Bitswarden < ActiveRecord::Base
	has_one :user, :foreign_key => :id, :primary_key => :bitswarden_id
	has_many :hostel_wardens, :primary_key => :bitswarden_id 
  attr_accessible :bitswarden_id, :name

end

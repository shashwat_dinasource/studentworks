class HostelWarden < ActiveRecord::Base
	belongs_to :bitswarden, :primary_key => :bitswarden_id
  attr_accessible :hostel, :bitswarden_id
end

class Leave < ActiveRecord::Base
	belongs_to :student, :primary_key => :student_id #defines primary key in student table
	
  attr_accessible :approved, :consent, :end, :levaddress, :phone, :reason, :start, :student_id, :bitswarden_id
  
end

class Address < ActiveRecord::Base
  belongs_to :student, :primary_key => :student_id
  attr_accessible :area, :house, :pin, :premise, :state, :student_id, :town
end

class Navigation < ActiveRecord::Base
  attr_accessible :label, :link, :position, :role
end

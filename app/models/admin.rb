class Admin < ActiveRecord::Base
	has_one :user, :foreign_key => :id, :primary_key => :admin_id
  attr_accessible :admin_id, :name
end

class Mess < ActiveRecord::Base
	belongs_to :student, :primary_key => :student_id
  attr_accessible :mess, :month, :student_id, :year
end

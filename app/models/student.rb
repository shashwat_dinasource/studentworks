class Student < ActiveRecord::Base
	#foreign key defined here defines "defined foreign key" column in insurance table will be searched.
	has_one :insurance, :primary_key => :student_id #primary key for student table
	has_one :address, :primary_key => :student_id
	has_many :leaves, :primary_key => :student_id
	has_many :bonafides, :primary_key => :student_id
	has_many :expenses, :primary_key => :student_id
	has_one :user, :foreign_key => :username, :primary_key => :student_id#defines primary key for user table while searching Student.find(1).user
  	has_many :messes, :primary_key =>:student_id; 
  attr_accessible :bloodgroup, :college_id, :dob, :email, :feepayingP, :fname, :hostel, :mname, :mobile, :name, :pdob, :plandline, :pmail, :pmobile, :psex, :relation, :room, :sex, :student_id,:incampus,:cgpa
end

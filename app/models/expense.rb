class Expense < ActiveRecord::Base
  belongs_to :student, :primary_key => :student_id
  attr_accessible :amount, :date, :reason, :student_id, :account, :total

def self.total(id)
	total=0
	 Student.find_by_student_id(id).expenses.each do |expense|
		if(expense.account=="credit")
			total= total + expense.amount
		else
			total= total - expense.amount
		end
	end
	return total
end 

end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130427004236) do

  create_table "addresses", :force => true do |t|
    t.string   "house"
    t.string   "premise"
    t.string   "area"
    t.string   "town"
    t.string   "state"
    t.integer  "pin"
    t.string   "student_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "admins", :force => true do |t|
    t.string   "admin_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "announcements", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "posted_by"
    t.boolean  "is_visible"
    t.boolean  "is_archived"
    t.boolean  "is_important"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "bitswardens", :force => true do |t|
    t.string   "bitswarden_id"
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "bonafides", :force => true do |t|
    t.string   "student_id"
    t.integer  "approved_by"
    t.string   "reason"
    t.boolean  "print"
    t.string   "year"
    t.boolean  "collected"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "checks", :force => true do |t|
    t.string   "post"
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "expenses", :force => true do |t|
    t.string   "student_id"
    t.string   "account"
    t.integer  "amount"
    t.string   "reason"
    t.date     "date"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "hostel_wardens", :force => true do |t|
    t.string   "hostel"
    t.string   "bitswarden_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "insurances", :force => true do |t|
    t.string   "policyno",   :limit => 30
    t.date     "sdate"
    t.date     "edate"
    t.string   "student_id", :limit => 30
    t.string   "password",   :limit => 30
    t.integer  "amount"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "leaves", :force => true do |t|
    t.string   "student_id"
    t.string   "reason"
    t.datetime "start"
    t.datetime "end"
    t.boolean  "approved"
    t.string   "bitswarden_id"
    t.string   "consent"
    t.string   "levaddress"
    t.string   "phone"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "comment"
  end

  create_table "messes", :force => true do |t|
    t.string   "student_id"
    t.string   "month"
    t.integer  "year"
    t.string   "mess"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "messoptions", :force => true do |t|
    t.string   "month"
    t.string   "year"
    t.boolean  "isopen"
    t.boolean  "isfeedback"
    t.boolean  "isleave"
    t.string   "commentmess"
    t.string   "commentleave"
    t.string   "commentfeedback"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "navigations", :force => true do |t|
    t.string   "link"
    t.string   "label"
    t.string   "role"
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "options", :force => true do |t|
    t.string   "option"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "staffs", :force => true do |t|
    t.string   "staff_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "students", :force => true do |t|
    t.string   "bloodgroup"
    t.string   "college_id"
    t.date     "dob"
    t.string   "email"
    t.string   "feepayingP"
    t.string   "fname"
    t.string   "hostel"
    t.string   "mname"
    t.string   "mobile"
    t.string   "name"
    t.date     "pdob"
    t.string   "plandline"
    t.string   "pmail"
    t.string   "pmobile"
    t.string   "psex"
    t.string   "relation"
    t.string   "room"
    t.string   "sex"
    t.string   "student_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.boolean  "incampus"
    t.string   "cgpa"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username"
    t.string   "role"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end

class CreateChecks < ActiveRecord::Migration
  def change
    create_table :checks do |t|
      t.string :post
      t.string :title
      t.timestamps
    end
  end
end

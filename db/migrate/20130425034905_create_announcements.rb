class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.string :title
      t.text :body
      t.string :posted_by
      t.boolean :is_visible
      t.boolean :is_archived
      t.boolean :is_important

      t.timestamps
    end
  end
end

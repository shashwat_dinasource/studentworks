class CreateLeaves < ActiveRecord::Migration
  def change
    create_table :leaves do |t|
      t.string :student_id
      t.string :reason
      t.datetime :start
      t.datetime :end
      t.boolean :approved
      t.string :bitswarden_id
      t.string :consent
      t.string :levaddress
      t.string :phone

      t.timestamps
    end
  end
end

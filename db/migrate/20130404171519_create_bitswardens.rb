class CreateBitswardens < ActiveRecord::Migration
  def change
    create_table :bitswardens do |t|
      t.string :bitswarden_id
      t.string :name

      t.timestamps
    end
  end
end

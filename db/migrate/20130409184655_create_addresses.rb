class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :house
      t.string :premise
      t.string :area
      t.string :town
      t.string :state
      t.integer :pin
      t.string :student_id

      t.timestamps
    end
  end
end

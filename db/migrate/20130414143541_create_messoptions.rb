class CreateMessoptions < ActiveRecord::Migration
  def change
    create_table :messoptions do |t|
      t.string :month
      t.string :year
      t.boolean :isopen
      t.boolean :isfeedback
      t.boolean :isleave
      t.string :commentmess
      t.string :commentleave
      t.string :commentfeedback

      t.timestamps
    end
  end
end

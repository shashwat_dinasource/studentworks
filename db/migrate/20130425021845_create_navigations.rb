class CreateNavigations < ActiveRecord::Migration
  def change
    create_table :navigations do |t|
      t.string :link
      t.string :label
      t.string :role
      t.integer :position

      t.timestamps
    end
  end
end

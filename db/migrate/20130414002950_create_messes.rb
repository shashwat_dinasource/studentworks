class CreateMesses < ActiveRecord::Migration
  def change
    create_table :messes do |t|
      t.string :student_id
      t.string :month
      t.integer :year
      t.string :mess

      t.timestamps
    end
  end
end

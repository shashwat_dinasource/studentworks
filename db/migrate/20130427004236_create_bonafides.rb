class CreateBonafides < ActiveRecord::Migration
  def change
    create_table :bonafides do |t|
      t.string :student_id
      t.boolean :approved
      t.integer :approved_by
      t.string :reason
      t.boolean :print
      t.string :year
      t.boolean :collected

      t.timestamps
    end
  end
end

class CreateStudents < ActiveRecord::Migration
	def change
    create_table :students do |t|
      
     t.string :bloodgroup 
     t.string :college_id 
     t.date :dob
     t.string :email
     t.string :feepayingP 
     t.string :fname
     t.string :hostel 
     t.string :mname 
     t.string :mobile
     t.string :name 
     t.date :pdob
     t.string :plandline 
     t.string :pmail
     t.string :pmobile 
     t.string :psex
     t.string :relation 
     t.string :room 
     t.string :sex 
     t.string :student_id

      t.timestamps
    end
end
end

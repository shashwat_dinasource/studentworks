class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :student_id
      t.string :account
      t.integer :amount
      t.string :reason
      t.date :date

      t.timestamps
    end
  end
end
